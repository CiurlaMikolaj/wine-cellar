const express = require('express');
const router = express.Router();
const checkAuth = require('../../middleware/check-auth.js');

const WinesController = require('../../controllers/wines.js');

router.get('/', WinesController.wines_get_all);

router.post('/', checkAuth, WinesController.wines_create_wine);

router.get('/:wineId', WinesController.wines_get_wine);

router.patch('/:wineId', checkAuth, WinesController.wines_update_wine);

router.delete('/:wineId', checkAuth, WinesController.wines_delete_wine);

module.exports = router;