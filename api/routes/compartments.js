const express = require('express');
const router = express.Router();
const checkAuth = require('../../middleware/check-auth.js');

const CompartmentsController = require('../../controllers/compartments.js');

router.get('/', CompartmentsController.compartments_get_all);

router.post('/', checkAuth, CompartmentsController.compartments_create_compartment);

router.get('/:compartmentId', CompartmentsController.compartments_get_compartment);

router.patch('/:compartmentId', checkAuth, CompartmentsController.compartments_update_compartment);

router.delete('/:compartmentId', checkAuth, CompartmentsController.compartments_delete_compartment);

module.exports = router;