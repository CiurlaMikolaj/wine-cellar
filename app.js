const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const wineRoutes = require('./api/routes/wines');
const orderRoutes = require('./api/routes/orders');
const userRoutes = require('./api/routes/user');
const compartmentRoutes = require('./api/routes/compartments');

mongoose.connect(
    'mongodb://node-cellar:'+ 
        process.env.MONGO_ATLAS_PW +
        '@node-rest-winecellar-shard-00-00-ds6yn.mongodb.net:27017,node-rest-winecellar-shard-00-01-ds6yn.mongodb.net:27017,node-rest-winecellar-shard-00-02-ds6yn.mongodb.net:27017/test?ssl=true&replicaSet=node-rest-winecellar-shard-0&authSource=admin&retryWrites=true&w=majority', 
    {
        useMongoClient: true
    }
);
mongoose.Promise = global.Promise;

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use((req, res, next) =>{
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers", 
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    if(req.method === 'OPTIONS'){
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json({});
    }
    next();
});

// Routes which should handle requests
app.use('/wines', wineRoutes);
app.use('/orders', orderRoutes);
app.use('/user', userRoutes);
app.use('/compartments', compartmentRoutes)

app.use((req, res, next) => {
    const error = new Error('Not found');
    error.status = 404;
    next(error);
})

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});

module.exports = app;