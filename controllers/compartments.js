const mongoose = require('mongoose');

const Compartment = require('../models/compartment.js');

exports.compartments_get_all = (req, res, next)=>{
    Compartment.find()
        .select('-__v')
        .exec()
        .then(docs => {
            const response = {
                count: docs.length,
                compartments: docs.map(doc =>{
                    return {
                        name: doc.name,
                        _id: doc._id,
                        request: {
                            type: 'GET',
                            url: 'http://localhost:3000/compartments/' + doc._id
                        }
                    }
                })
            };
            res.status(200).json(response);
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
}

exports.compartments_create_compartment = (req, res, next)=>{
    const compartment = new Compartment({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
    });
    compartment
        .save()
        .then(result => {
            console.log(result);
            res.status(201).json({
                message: 'Created compartment succesfully',
                createdCompartment: {
                    name: result.name,
                    _id: result._id,
                    request:{
                        type: 'GET',
                        url: 'http://localhost:3000/compartments/' + result._id
                    }
                }
            });
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
}

exports.compartments_get_compartment = (req, res, next)=>{
    const id = req.params.compartmentId;
    Compartment.findById(id)
        .select('-__v')
        .exec()
        .then(doc => {
            console.log("From database", doc);
            if(doc){
                res.status(200).json({
                    compartment: doc,
                    request: {
                        type: 'GET',
                        url: 'http://localhost:3000/compartments'
                    }
                });
            } else {
                res.status(404).json({message: 'Not valid entry found for provided ID'});
            }
            
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({error: err});
        });
}

exports.compartments_update_compartment = (req, res, next)=>{
    const id = req.params.compartmentId;
    const updateOps = {};
    for(const ops of req.body){
        updateOps[ops.propName] = ops.value;
    }
    Compartment.update({_id: id}, {$set: updateOps})
        .exec()
        .then(result => {
            res.status(200).json({
                message: 'Compartment updated',
                request: {
                    type:'GET',
                    url: 'http://localhost:3000/compartments/' + id
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
}

exports.compartments_delete_compartment = (req, res, next)=>{
    const id = req.params.compartmentId;
    Compartment.remove({_id: id})
        .exec()
        .then(result =>{
            res.status(200).json({
                message: 'Compartment deleted',
                request: {
                    type: 'POST',
                    url: 'http://localhost:3000/compartments',
                    body: { name: 'String' }
                }
            })
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
}