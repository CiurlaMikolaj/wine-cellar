const mongoose = require('mongoose');

const Order = require('../models/order.js');
const Wine = require('../models/wine.js');


exports.orders_get_all = (req, res, next)=>{
    Order
    .find()
    .select('-__v')
    .populate('wine', 'name')
    .exec()
    .then(docs => {
        res.status(200).json({
            count: docs.length,
            orders: docs.map(doc => {
                return{
                    _id: doc._id,
                    title: doc.title,
                    wine: doc.wine,
                    quantity: doc.quantity,
                    deadline: doc.deadline,
                    status: doc.status,
                    request: {
                        type: 'GET',
                        url: 'http://localhost:3000/orders/' + doc._id
                    }
                }
            })
        });
    })
    .catch(err => {
        res.status(500).json({
            error: err
        })
    });
}

exports.orders_create_order = (req, res, next)=>{
    Wine.findById(req.body.wineId)
        .then(wine => {
            if(!wine) {
                return res.status(404).json({
                    message: 'Wine not found'
                });
            }
            const order = new Order({
                _id: mongoose.Types.ObjectId(),
                title: req.body.title,
                quantity: req.body.quantity,
                deadline: req.body.deadline,
                status: req.body.status,
                wine: req.body.wineId
            });
            return order.save()
        })
        .then(result => {
            console.log(result);
            res.status(201).json({
                message: 'Order stored',
                createdOrder: {
                    _id: result._id,
                    title: result.title,
                    wine: result.wine,
                    quantity: result.quantity,
                    deadline: result.deadline,
                    status: result.status
                },
                request: {
                    type: 'GET',
                    url: 'http://localhost:3000/orders/' + result._id
                }
            });
        })
        .catch(err => {
            res.status(500).json({
                error: err
            });
        });
}

exports.orders_get_order = (req, res, next)=>{
    Order.findById(req.params.orderId)
        .select('-__v')
        .populate('wine')
        .exec()
        .then(order => {
            if(!order){
                return res.status(404).json({
                    message: 'Order not found'
                })
            }
            res.status(200).json({
                order: order,
                request: {
                    type: 'GET',
                    url: 'http://localhost:3000/orders'
                }
            });
        })
        .catch(err => {
            res.status(500).json({
                error: err
            });
        });
}

exports.orders_update_order = (req, res, next)=>{
    const id = req.params.orderId;
    const updateOps = {};
    for(const ops of req.body){
        updateOps[ops.propName] = ops.value;
    }
    Order.update({_id: id}, {$set: updateOps})
        .exec()
        .then(result => {
            res.status(200).json({
                message: 'Order updated',
                request: {
                    type:'GET',
                    url: 'http://localhost:3000/orders/' + id
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
}

exports.orders_delete_order = (req, res, next)=>{
    Order.remove({
        _id: req.params.orderId
    })
    .exec()
    .then(result => {
        res.status(200).json({
            message: 'Order deleted',
            request: {
                type: 'POST',
                url: 'http://localhost:3000/orders',
                body: { wineId: 'ID', quantity: 'Number' }
            }
        });
    })
    .catch(err => {
        res.status(500).json({
            error: err
        });
    });
}