const mongoose = require('mongoose');

const Wine = require('../models/wine.js');
const Compartment = require('../models/compartment.js');

exports.wines_get_all = (req, res, next)=>{
    Wine.find()
        .select('-__v')
        .populate('compartment', 'name')
        .exec()
        .then(docs => {
            const response = {
                count: docs.length,
                wines: docs.map(doc =>{
                    return {
                        name: doc.name,
                        type: doc.type,
                        year: doc.year,
                        price: doc.price,
                        _id: doc._id,
                        compartment: doc.compartment,
                        request: {
                            type: 'GET',
                            url: 'http://localhost:3000/wines/' + doc._id
                        }
                    }
                })
            };
            res.status(200).json(response);
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
}

exports.wines_create_wine = (req, res, next)=>{
    Compartment.findById(req.body.compartmentId)
        .then(compartment => {
            if(!compartment) {
                return res.status(404).json({
                    message: 'Compartment not found'
                });
            }
            const wine = new Wine({
                _id: mongoose.Types.ObjectId(),
                name: req.body.name,
                type: req.body.type,
                year: req.body.year,
                price: req.body.price,
                compartment: req.body.compartmentId
            });
            return wine.save()
        })
        .then(result => {
            console.log(result);
            res.status(201).json({
                message: 'Wine stored',
                createdWine: {
                    _id: result._id,
                    compartment: result.compartment,
                    name: result.name,
                    type: result.type,
                    year: result.year,
                    price: result.price
                },
                request:{
                    type: 'GET',
                    url: 'http://localhost:3000/wines/' + result._id
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
}

exports.wines_get_wine = (req, res, next)=>{
    const id = req.params.wineId;
    Wine.findById(id)
        .select('-__v')
        .populate('compartment')
        .exec()
        .then(wine => {
            if(!wine){
                return res.status(404).json({
                    message: 'Wine not found'
                })
            }
            res.status(200).json({
                wine: wine,
                request: {
                    type: 'GET',
                    url: 'http://localhost:3000/wines'
                }
            });
        })
        .catch(err => {
            res.status(500).json({
                error: err
            });
        });
}

exports.wines_update_wine = (req, res, next)=>{
    const id = req.params.wineId;
    const updateOps = {};
    for(const ops of req.body){
        updateOps[ops.propName] = ops.value;
    }
    Wine.update({_id: id}, {$set: updateOps})
        .exec()
        .then(result => {
            res.status(200).json({
                message: 'Product updated',
                request: {
                    type:'GET',
                    url: 'http://localhost:3000/wines/' + id
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
}

exports.wines_delete_wine = (req, res, next)=>{
    const id = req.params.wineId;
    Wine.remove({_id: id})
        .exec()
        .then(result =>{
            res.status(200).json({
                message: 'Wine deleted',
                request: {
                    type: 'POST',
                    url: 'http://localhost:3000/wines',
                    body: { name: 'String', price: 'Number', compartment: 'ID' }
                }
            })
        })
        .catch(err => {
            res.status(500).json({
                error: err
            });
        });
}