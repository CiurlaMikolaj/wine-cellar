const mongoose = require('mongoose');

const wineSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    compartment: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Compartment', 
        required: true 
    },
    name: {
        type: String, 
        required: true
    },
    type: {type: String, 
        default: "Unspecified"
    },
    price: {
        type: Number, 
        required: true
    },
    year: { 
        type: Date, 
        required: true
    }
});

module.exports = mongoose.model('Wine', wineSchema);