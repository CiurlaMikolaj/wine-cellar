const mongoose = require('mongoose');

const orderSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    wine: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Wine', 
        required: true 
    },
    title: {
        type: String, 
        required: true
    },
    status: {
        type: String, 
        enum: ['NEW', 'COMPLETED', 'ABANDONED'],
        default: 'NEW'
    },
    deadline: { 
        type: Date, 
        default:  "2020-02-09" 
    },
    quantity: { 
        type: Number, 
        default: 1 
    }
});

module.exports = mongoose.model('Order', orderSchema);