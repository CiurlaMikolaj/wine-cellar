const mongoose = require('mongoose');

const compartmentSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        type: String, 
        required: true
    }
});

module.exports = mongoose.model('Compartment', compartmentSchema);